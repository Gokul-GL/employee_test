import os
from flask import Flask,request,url_for,render_template,redirect,flash
import mysql.connector

mydb=mysql.connector.connect(
    host="127.0.0.1",
    user="root",
    password="",
    database="empdetails"
)
app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key=os.urandom(24)
app.config.from_envvar("FLASKR_SETTINGS",silent=True)

@app.route("/",methods=["GET","POST"])
def home():
    db=mydb.cursor()
    if request.args.get('orderby'):
        orderby = request.args.get('orderby')
        db.execute('SELECT * from employee order by '+orderby+' desc;')
    elif request.method=="POST":
        if request.form['orderby']!="":
            orderby = request.form['orderby']
            db.execute('SELECT * from employee order by '+orderby+' desc;')
        else:
            db.execute('SELECT * from employee;')
    else:
        db.execute('SELECT * from employee;')
    #db.execute("SELECT * from employee")
    myresults=db.fetchall()
    print(myresults)
    return render_template("employee.html",myresult=myresults)

if __name__=="__main__":
    app.run(debug=True)