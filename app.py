from flask import Flask, render_template, request, redirect, url_for
from flask_mysqldb import MySQL
import datetime
now = datetime.datetime.now()
app = Flask(__name__)

app.config['MySQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'employee'

mysql = MySQL(app)


@app.route('/',methods=['GET', 'POST'])
def index():
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        dataOrder = request.form['data_type']
        print(dataOrder, "Nothing")
        resultValue = cur.execute("Select * from employee order by "+dataOrder+"")
    else:
        resultValue = cur.execute("Select * from employee order by id")
    if resultValue > 0:
        employeeDetails = cur.fetchall()
        print(employeeDetails)
        return render_template('index.html', data=employeeDetails)
    else:
        print("No Records Found")
        return render_template("index.html")




@app.route("/Employee/<string:id>", methods=['GET', 'POST'])
def add_EditEmployeeDetails(id):
    print(id)
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        name = request.form['employee_name']
        address = request.form['employee_address']
        gender = request.form['gender']
        joineddate = request.form['join_date']
        updatedtime = now.strftime("%Y-%m-%d %H:%M:%S")
        print(id)
        if id == "0":
            cur = mysql.connection.cursor()
            cur.execute(
                "insert into employee(employe_name,employe_address,employe_gender,join_date,update_time) values(%s,%s,%s,%s,%s)",
                (name, address, gender, joineddate, updatedtime))
        else:
            print("Update Employee")
            sql = "update employee set employe_name=%s,employe_address=%s,employe_gender=%s,join_date=%s,update_time=%s where Id=%s"
            cur.execute(sql, [name, address, gender, joineddate, updatedtime, id])
        mysql.connection.commit()
        cur.close()
        return redirect(url_for("index"))

    return render_template("Employee.html")


# Delete Employee Details
@app.route("/deleteEmployeeDetails/<string:id>", methods=['GET', 'POST'])
def deleteEmployeeDetails(id):
    cur = mysql.connection.cursor()
    sql = "delete employee,salary from employee INNER JOIN  salary on salary.employee_id=employee.Id where employee.Id=%s"
    cur.execute(sql, id)
    mysql.connection.commit()
    cur.close()

    return redirect(url_for("index"))


# Add Salary Details
@app.route("/salaryDetails/<string:id>", methods=['GET', 'POST'])
def salaryDetails(id):
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        dataOrder = request.form['data_type']
        print(dataOrder, "Nothing")
        sql = "select * from salary where employee_id=%s order by "+dataOrder+""
        resultValue = cur.execute(sql, id)
    else:
        sql = "select * from salary where employee_id=%s order by id"
        resultValue = cur.execute(sql, id)

    if resultValue > 0:
        salaryDetails = cur.fetchall()
        print("Salary Details", salaryDetails)
        return render_template('salarydetails.html', data=salaryDetails)
    else:
        print("No Records Found")
        return render_template('salarydetails.html')



@app.route("/salary/<string:id>", methods=['GET', 'POST'])
def add_EditSalaryDetails(id):
    print(id)
    if request.method == 'POST':
        cur = mysql.connection.cursor()
        employee_id=request.form['employee_id']
        salary_month = request.form['salary_month']
        salary = request.form['salary']
        create_time = now
        updatedtime = now.strftime("%Y-%m-%d %H:%M:%S")
        print(id)
        if id == "0":
            cur = mysql.connection.cursor()
            cur.execute(
                "insert into salary(employee_id,salary,salary_month,create_time,update_time) values(%s,%s,%s,%s,%s)",
                (employee_id, salary, salary_month, create_time, updatedtime))
        else:
            print("Update Employee")
            sql = "update salary set salary=%s,salary_month=%s,update_time=%s where Id=%s"
            cur.execute(sql, [salary, salary_month, updatedtime, id])
        mysql.connection.commit()
        cur.close()
        return redirect(url_for("salaryDetails", id=employee_id))

    return render_template("Salary.html")

# Delete Salary Details
@app.route("/deleteESalaryDetails/<string:id>,<string:employee_id>", methods=['GET', 'POST'])
def deleteSalaryDetails(id,employee_id):
    print(id)
    emp_id=employee_id
    cur = mysql.connection.cursor()
    sql = "delete from salary where Id=%s"
    cur.execute(sql, id)
    mysql.connection.commit()
    cur.close()

    return redirect(url_for("salaryDetails",id=emp_id))

@app.route("/report2/",methods=['GET', 'POST'])
def report2():
    dataOrder = 1
    cur = mysql.connection.cursor()
    sql_name=cur.execute("select * from employee order by Id")
    employeeName = cur.fetchall()
    employee_salaryDetails = []
    print(employeeName)
    if request.method == 'POST':
        dataOrder = request.form['data_type']
        employee_salaryDetails.append(dataOrder)
        print(dataOrder, "Nothing")
        sql="Select salary from salary where employee_id=%s"
        resultValue = cur.execute(sql,[dataOrder])
        #sql="select salary.salary from salary inner join employee on "
    else:
        resultValue = cur.execute("Select salary from salary where employee_id=1")
        employee_salaryDetails.append(dataOrder)
    if resultValue > 0:
        employeeSalary = cur.fetchall()
        print(employeeSalary)
        employee_salaryDetails.append(min(employeeSalary))
        employee_salaryDetails.append(max(employeeSalary))
        tot = 0
        for row in employeeSalary:
            tot += row[0]
        print("length",len(employeeSalary))
        employee_salaryDetails.append(tot)
        employee_salaryDetails.append(tot/len(employeeSalary))
        print(employee_salaryDetails)
        #return render_template('Report2.html', data=employee_salaryDetails,e_name=employeeName)
    else:
        print("No Records Found")
        #return render_template("Report2.html",e_name=employeeName,data=employee_salaryDetails)
    return render_template("Report2.html",e_name=employeeName,data=employee_salaryDetails)

@app.route("/report1/",methods=['GET', 'POST'])
def report1():
    cur = mysql.connection.cursor()
    if request.method=='POST':
        start_month=request.form['start_month']
        end_month=request.form['end_month']
        sql="SELECT employee_id,salary,salary_month FROM salary WHERE salary_month BETWEEN %s AND %s"
        result=cur.execute(sql,[start_month,end_month])
        print(result)
        if result>0:
            finalValue=cur.fetchall()
            print("Filtered Employee Details ",finalValue)
        return render_template("Report1.html")
    else:
        return render_template("Report1.html")

    return render_template("Report1.html")

if __name__ == '__main__':
    app.run()

# OLD Code
# # Add new Employee Record
# @app.route("/addNewEmployee", methods=['GET', 'POST'])
# def addNewEmployee():
#     if request.method == 'POST':
#         cur = mysql.connection.cursor()
#         name = request.form['employee_name']
#         address = request.form['employee_address']
#         gender = request.form['gender']
#         joineddate = request.form['join_date']
#         updatedtime = now.strftime("%Y-%m-%d %H:%M:%S")
#         cur.execute(
#             "insert into employee(employe_name,employe_address,employe_gender,join_date,update_time) values(%s,%s,%s,%s,%s)",
#             (name, address, gender, joineddate, updatedtime))
#         mysql.connection.commit()
#         cur.close()
#
#         return redirect(url_for("index"))
#     else:
#         print("Data Not Inserted")
#     return render_template("Employee.html")
#
#
# # update Employee Details
# @app.route("/updateEmployeeDetails/<string:id>", methods=['GET', 'POST'])
# def updateEmployeeDetails(id):
#     print("Employee id", id)
#     if request.method == 'POST':
#         cur = mysql.connection.cursor()
#         name = request.form['employee_name']
#         address = request.form['employee_address']
#         gender = request.form['gender']
#         joineddate = request.form['join_date']
#         updatedtime = now.strftime("%Y-%m-%d %H:%M:%S")
#         sql = "update employee set employe_name=%s,employe_address=%s,employe_gender=%s,join_date=%s,update_time=%s where Id=%s"
#         cur.execute(sql, [name, address, gender, joineddate, updatedtime, id])
#         mysql.connection.commit()
#         cur.close()
#
#         return redirect(url_for("index"))
#
#     cur = mysql.connection.cursor()
#     sql = "select * from employee where Id=%s"
#     cur.execute(sql, [id])
#     employeeDetails = cur.fetchone()
#     print("Employee Details", employeeDetails)
#     return render_template("updateEmployeeDetails.html", employeeDetails=employeeDetails)
