import os
import mysql.connector
from flask import Flask,request,g,redirect,url_for,render_template,flash,session
from functools import wraps

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="employee"
)

mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE employee (Id INT NOT NULL AUTO_INCREMENT,name VARCHAR(255), address VARCHAR(255),Gender varchar(10),JoinDate Date,CreateTime DATETIME DEFAULT CURRENT_TIMESTAMP,updateTime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,PRIMARY KEY (Id))")
mycursor.execute("CREATE TABLE Salary (Id INT NOT NULL AUTO_INCREMENT,employeeId int , salary int,month varchar(10),CreateTime DATETIME DEFAULT CURRENT_TIMESTAMP,updateTime DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,PRIMARY KEY (Id),FOREIGN KEY (employeeId) REFERENCES employee(Id))")
